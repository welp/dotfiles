#!/usr/bin/env fish

set -gx EDITOR "emacsclient -t -a emacs"
# set -gx TERM tmux-256color

# Prefer US English and use UTF-8.
set -gx LANG "en_US.UTF-8"
set -gx LC_ALL "en_US.UTF-8"

# upstream 9d30bd673c72ed1cadb2720fe4fb44a8ce915a2b
# Enable persistent REPL history for `node`.
set -gx NODE_REPL_HISTORY_FILE ~/.node_history
# Allow 32³ entries; the default is 1000.
set -gx NODE_REPL_HISTORY_SIZE 32768

# Highlight section titles in manual pages.
set SOLAR_BLUE (tput setaf 33)
set -gx LESS_TERMCAP_md $SOLAR_BLUE

# Don’t clear the screen after quitting a manual page.
set -gx MANPAGER "less -X"

# GPG
# ====
# if [ -f "${HOME}/.gpg-agent-info" ]; then
#   . "${HOME}/.gpg-agent-info"
#   set -gx GPG_AGENT_INFO
# fi
set -gx GPG_TTY (tty)

# SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
# set -gx SSH_AUTH_SOCK
