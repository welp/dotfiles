#!/usr/bin/env fish

alias ff="firefox 2>/dev/null >&2"
alias itunes="ncmpcpp"
alias music="sudo mpd --stdout -v --no-daemon ~/.mpd/mpd.conf"
alias nixpath="nix-build '<nixpkgs>' --no-build-output -A"
alias now='date "+%Y-%m-%d-%H%M%S"'
alias reload="exec "$SHELL" -l"
alias scrobble="mpdscribble --conf ~/.mpd/mpdscribble.conf --no-daemon"
alias slack="slack 2>/dev/null >&2"
alias today='date "+%Y-%m-%d"'
alias vivaldi='vivaldi-stable'
