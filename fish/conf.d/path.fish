set -U fish_user_paths \
        $HOME"/bin" \
        $HOME"/.cargo/bin" \
        $HOME"/.npm-packages/bin" \
        $HOME"/.rbenv/shims" \
        $HOME"/.local/bin" \
        "/usr/local/bin" \
        "/usr/local/sbin" \
        "/home/linuxbrew/.linuxbrew/bin" \
        "/home/linuxbrew/.linuxbrew/sbin" \
        "/bin" \
        "/sbin" \
        "/usr/bin" \
        "/usr/sbin" \
        $PATH
