#!/usr/bin/env fish

function nixruby
  set freedesktop (nixpath shared-mime-info)
  set run "CFLAGS=-Wno-error=format-overflow FREEDESKTOP_MIME_TYPES_PATH="$freedesktop"/share/mime/packages/freedesktop.org.xml "(string join ' ' $argv)""

  command nix-shell -p \
            zlib \
            libxml2 \
            postgresql \
            sqlite \
            libxslt \
            shared-mime-info \
            --run $run
end
