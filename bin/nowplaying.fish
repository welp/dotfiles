#!/usr/bin/env fish

function printsong
    mpc current --format '“%title%” by %artist%' 2>/dev/null | tee ~/stream/playing.txt
end

while mpc 2>/dev/null;
    mpc idle player && printsong
end
