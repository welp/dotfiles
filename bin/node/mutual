#!/usr/bin/env node

(async function () {
  const credentials = {
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SEC,
    access_token: process.env.TWITTER_ACCESS_TOKEN,
    access_token_secret: process.env.TWITTER_ACCESS_SECRET,
  }

  const Twit = require('twit')
  const T = new Twit(credentials)

  const args = process.argv
  if (args.length < 4) {
    console.error('Error [mutual]: Bad # of arguments')
    console.log("Usage: `mutual darth '<>' duckthecat [--simple]`")
  }
  else {
    const simple = !!args[5]

    // "darth '<<' duckthecat" — darth's followers that duck follows
    // "darth '<>' duckthecat" — darth's followers that follow duck
    // "darth '>>' duckthecat" — duck's followers that darth follows
    // "darth '><' duckthecat" — people that darth follows that duck follows
    const user1 = (args[3].charAt(0) === '>' ? 'friends' : 'followers')
    const user2 = (args[3].charAt(1) === '<' ? 'friends' : 'followers')

    const values = await Promise.all([
      creep(args[2], user1, T),
      creep(args[4], user2, T),
    ])
    const all = values[0].concat(values[1])

    // http://stackoverflow.com/questions/840781/easiest-way-to-find-duplicate-values-in-a-javascript-array
    const sorted = all.sort()
    let i = all.length
    const mutuals = []
    while (i--)
      if (sorted[i + 1] === sorted[i])
        mutuals.push(sorted[i])

    if (!simple) {
      const n = mutuals.length
      let output1 = `@${args[2]}`
      output1 += (user1 === 'friends' ? ' is following' : ' is followed by')
      let output2 = `@${args[4]}`
      output2 += (user2 === 'friends' ? ' is following' : ' is followed by')
      console.log(`${output1} ${n} people that ${output2}`)
    }

    while (mutuals.length) {
      let lookupLimit = (mutuals.length < 100 ?
                         mutuals.length : 100 )
      let lookups = ''
      while (lookupLimit--) { lookups += `,${mutuals.pop()}` }
      lookups = lookups.slice(1)

      let lookupData = await T.post('users/lookup', { user_id: lookups, })
      let data = lookupData.data

      let k = data.length
      while (k--) {
        if (simple)
          console.log(data[k].screen_name)
        else
          console.log(`${data[k].name} (@${data[k].screen_name})`)
      }
    }
  }
})()

// @param user String twitter username
// @param direction String either 'friends' or 'followers'
// @param twitter Twit
async function creep(user, direction, twitter) {
  const opts = { screen_name: user, count: 5000, }

  const idData = await twitter.get(`${direction}/ids`, opts)

  // console.dir(idData.resp.toJSON())
  console.log(`remaining ${direction} requests: ${idData.resp.headers['x-rate-limit-remaining']}`)
  // console.dir(idData.data)

  let ids = idData.data.ids
  let cursor = idData.data.next_cursor_str

  while (cursor !== '0') {
    let pagination = {
      cursor: cursor,
      screen_name: user,
      count: 5000,
    }

    let pagedData = await twitter.get(`${direction}/ids`, pagination)

    ids = ids.concat(pagedData.data.ids)
    cursor = pagedData.data.next_cursor_str
  }

  return ids
}
