#!/usr/bin/env fish

set namespace $argv[1]

helm list -n $namespace | awk '{ print $1; }' | sed '1d' | xargs -n1 helm uninstall -n $namespace
