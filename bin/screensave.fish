#!/usr/bin/env fish

grim -g (slurp) ~/Desktop/(date '+ %s' | tr -d ' ').png
