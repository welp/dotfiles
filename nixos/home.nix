{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  home.packages = [
    pkgs.alacritty
    pkgs.awscli
    pkgs.bash-completion
    pkgs.bind
    pkgs.bundix
    pkgs.editorconfig-core-c
    pkgs.emacs
    pkgs.file
    pkgs.fish
    pkgs.gimp
    pkgs.gitAndTools.pass-git-helper
    pkgs.git-secrets
    pkgs.gnumake
    pkgs.imagemagick
    pkgs.inconsolata
    pkgs.jdk
    pkgs.jq
    pkgs.k9s
    pkgs.killall
    pkgs.kube3d
    pkgs.kubectl
    pkgs.kubernetes-helm
    pkgs.libxml2
    pkgs.libxslt
    pkgs.lsof
    pkgs.mime-types
    pkgs.mysql57
    pkgs.ncat
    pkgs.nodejs
    pkgs.openssl
    pkgs.openvpn
    pkgs.pandoc
    pkgs.pass
    pkgs.pinentry
    pkgs.postgresql
    pkgs.python3
    pkgs.ripgrep
    pkgs.ruby
    pkgs.rustup
    pkgs.shared-mime-info
    pkgs.shellcheck
    pkgs.silver-searcher
    pkgs.slack
    pkgs.source-code-pro
    pkgs.sqlite
    pkgs.tig
    pkgs.tmux
    pkgs.tree
    pkgs.unzip
    pkgs.xclip
    pkgs.zlib
    pkgs.zoom-us
  ];

  programs.gpg.enable = true;
  programs.git.enable = true;
  programs.firefox.enable = true;

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "20.03";
}
